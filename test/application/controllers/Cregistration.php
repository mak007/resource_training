<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cregistration extends CI_Controller {

	
	public function userRegistration()
	{
		$this->load->view('User_registration');	
	}
	
	public function insertRegistration()
	{
	 $data = array(
			 		'firstname'=>$this->input->POST('firstname'),
					'lastname'=>$this->input->POST('lastname'),
			 		'address'=>$this->input->POST('address'),
			 		'email'=>$this->input->POST('email'),
			 		'contactno'=>$this->input->POST('contactno'),
			 		'password'=>$this->input->POST('password'),
			 		'city'=>$this->input->POST('city'),
			 		);
		$this->load->model('Mregistration');
		$this->load->database();
		$this->Mregistration->saveRegistration($data);
		$this->load->view('VLogin_page');
	
	}
	
}
